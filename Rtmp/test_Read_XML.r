#' $Id: test_Read_XML.r 85 2018-03-16 07:49:11Z Christian.Vonderach $
#' try to read XML file into R and produce a data.frame

require("XML")
## setwd() should point to "H:/FVA-Projekte/HE-VSB"

#### transform STM-File into data.frame and save into csv ####
# datpath <- file.path(getwd(), "Daten/Urdaten/Kaelberbronn/Ponsse/kaelberbronn_stm.XML")
f <- "Stm_Schlauchweg.XML"
p <- "Daten/Urdaten/Schönberg_Schlauchweg"
datpath <- file.path(getwd(), p, f)
file.exists(datpath)
dat <- xmlParse(datpath)

dlist <- xmlToList(dat) #takes some time
length(dlist)
str(dlist[[1]], max.level = 1) # structure of STMData
str(dlist[[34553]], max.level = 1) # structure of STMDataDetails
length(dlist)

unique(names(dlist))
tree_list <- dlist[which(names(dlist) == "STMData")]
tree_df <- data.frame(as.vector(t(sapply(tree_list, c))), row.names = NULL) # 'c' stands for concatenate, check ?c
names(tree_list[[1]])

tree_df <- data.frame(t(sapply(tree_list, function(a){as.vector(unlist(a))})), 
                      row.names = NULL, stringsAsFactors = F)
colnames(tree_df) <- names(tree_list[[1]])[which(!sapply(tree_list[[1]], is.null))]
tree_df$Stammnummer <- as.numeric(tree_df$Stammnummer)
tree_df$BaumartCode <- as.numeric(tree_df$BaumartCode)
tree_df$BHD <- as.numeric(tree_df$BHD)
tree_df$BaumartCode <- as.numeric(tree_df$BaumartCode)
str(tree_df)
head(tree_df)

taper_list <- dlist[which(names(dlist) == "STMDataDetails")]
taper_df <- data.frame(t(sapply(taper_list, function(a){as.vector(unlist(a))})),
                       row.names = NULL, stringsAsFactors = F)
colnames(taper_df) <- c("ID_STMData", "Laenge", "Durchmesser", "SortimentName", "SortimentCode")
taper_df$Laenge <- as.numeric(taper_df$Laenge)
taper_df$Durchmesser <- as.numeric(taper_df$Durchmesser)
taper_df$SortimentCode <- as.numeric(taper_df$SortimentCode)
str(taper_df)
head(taper_df)

write.csv2(tree_df, file.path(getwd(), p, "trees.csv"), row.names = F)
write.csv2(taper_df, file.path(getwd(), p, "taper.csv"), row.names = F)
##ok

id <- taper_df$ID_STMData[[1]]
plot(Durchmesser ~ Laenge, data = subset(taper_df, ID_STMData == id), 
     type="l", main = paste("Baumnr:",id))


#### also transform PRD-File into data.frame and save into csv ####
## setwd() should point to "H:/FVA-Projekte/HE-VSB"
datpath <- file.path(getwd(), "Daten/Urdaten/Kaelberbronn/Ponsse/kaelberbronn_prd.XML")
file.exists(datpath)
dat <- xmlParse(datpath)

dlist <- xmlToList(dat)
length(dlist)
str(dlist[[1]], max.level = 1)
length(dlist)

unique(names(dlist))
sort_list <- dlist[which(names(dlist) == "PRDData")]
sort_df <- data.frame(as.vector(t(sapply(sort_list, c))), row.names = NULL) # 'c' stands for concatenate, check ?c
names(sort_list[[1]])

sort_df <- data.frame(t(sapply(sort_list, function(a){as.vector(unlist(a))})), 
                      row.names = NULL, stringsAsFactors = F)
## encoding might be unset (somehow does not recognize german umlauts)
## work around to set encoding correctly for **column names** and **species names**
## https://stackoverflow.com/questions/27022767/reading-xml-files-in-r-utf-8-and-umlaut
columnNames <- as.character(names(sort_list[[1]])[which(!sapply(sort_list[[1]], is.null))])
Encoding(columnNames) <- "UTF-8"
colnames(sort_df) <- columnNames

## transform columns to correct data type and set encoding correctly
sort_df$BaumartIndex <- as.numeric(sort_df$BaumartIndex)
Encoding(sort_df$BaumartName) <- "UTF-8"
sort_df$BaumartCode <- as.numeric(sort_df$BaumartCode)
sort_df$SortimentIndex <- as.numeric(sort_df$SortimentIndex)
Encoding(sort_df$SortimentName) <- "UTF-8"
sort_df$SortimentCode <- as.numeric(sort_df$SortimentCode)
sort_df$Längenklasse <- as.numeric(sort_df$Längenklasse)
sort_df$Durchmesserklasse <- as.numeric(sort_df$Durchmesserklasse)
Encoding(sort_df$Stärkeklasse) <- "UTF-8"
sort_df$Stückzahl <- as.numeric(sort_df$Stückzahl)
sort_df$Volumen <- as.numeric(sort_df$Volumen)
sort_df$LängenklasseIndex <- as.numeric(sort_df$LängenklasseIndex)
sort_df$Preistyp <- as.numeric(sort_df$Preistyp)
sort_df$Baumzahl <- as.numeric(sort_df$Baumzahl)
str(sort_df)

##replace umlauts
replace_str <- c('ü' = 'ue', 'ï' = 'ie', 'ë' = 'ee', 'ä' = 'ae','ö' = 'oe',
                 'Ü' = 'Ue', 'Ä' = 'Ae', 'Ö' = 'Oe', 'ß' = 'ss')
stringr::str_replace_all('üïëäöÄÜÖß', replace_str)

colnames(sort_df) <- stringr::str_replace_all(colnames(sort_df), replace_str)
sort_df$BaumartName <- stringr::str_replace_all(sort_df$BaumartName, replace_str)
sort_df$SortimentName <- stringr::str_replace_all(sort_df$SortimentName, replace_str)
sort_df$Staerkeklasse <- stringr::str_replace_all(sort_df$Staerkeklasse, replace_str)

write.csv2(sort_df, file = file.path(getwd(), "Daten/aufbereiteteDaten/Kaelberbronn/sort.csv"), row.names = F)

#### transfrom PRI-File into data.frame and save into csv ####
datpath <- file.path(getwd(), "Daten/Urdaten/Kaelberbronn/Ponsse/kaelberbronn_pri.XML")
file.exists(datpath)
dat <- xmlParse(datpath)

dlist <- xmlToList(dat)
length(dlist)
str(dlist[[1]], max.level = 1)
length(dlist)

unique(names(dlist))
sort_list <- dlist[which(names(dlist) == "PRIData")]
sort_df <- data.frame(as.vector(t(sapply(sort_list, c))), row.names = NULL) # 'c' stands for concatenate, check ?c
names(sort_list[[1]])

# last stamm-entry erroneous (entry 1029-1032, stamm 222 (no bhd, no coordinates))
sort_df <- data.frame(t(sapply(1:1028, function(a){as.vector(unlist(sort_list[[a]]))})), 
                      row.names = NULL, stringsAsFactors = F)
## encoding might be unset (somehow does not recognize german umlauts)
## work around to set encoding correctly for **column names** and **species names**
## https://stackoverflow.com/questions/27022767/reading-xml-files-in-r-utf-8-and-umlaut
columnNames <- as.character(names(sort_list[[1]])[which(!sapply(sort_list[[1]], is.null))])
Encoding(columnNames) <- "UTF-8"
colnames(sort_df) <- columnNames

## transform columns to correct data type and set encoding correctly
str(sort_df)
sort_df$Stammnummer <- as.numeric(sort_df$Stammnummer)
sort_df$Stücknummer <- as.numeric(sort_df$Stücknummer)
Encoding(sort_df$Baumart) <- "UTF-8"
sort_df$BaumartCode <- as.numeric(sort_df$BaumartCode)
Encoding(sort_df$Sortiment) <- "UTF-8"
sort_df$SortimentCode <- as.numeric(sort_df$SortimentCode)
sort_df$ZopfDurchmesser_mit_Rinde <- as.numeric(sort_df$ZopfDurchmesser_mit_Rinde)
sort_df$ZopfDurchmesser_ohne_Rinde <- as.numeric(sort_df$ZopfDurchmesser_ohne_Rinde)
sort_df$MittenDurchmesser_mit_Rinde <- as.numeric(sort_df$MittenDurchmesser_mit_Rinde)
sort_df$MittenDuchmesser_ohne_Rinde <- as.numeric(sort_df$MittenDuchmesser_ohne_Rinde)
sort_df$MittenDurchmesserHKS_mit_Rinde <- as.numeric(sort_df$MittenDurchmesserHKS_mit_Rinde)
sort_df$MittenDurchmesserHKS_ohne_Rinde <- as.numeric(sort_df$MittenDurchmesserHKS_ohne_Rinde)
sort_df$VolumenPreistyp <- as.numeric(sort_df$VolumenPreistyp)
sort_df$VolumenSektion_mit_Rinde <- as.numeric(sort_df$VolumenSektion_mit_Rinde)
sort_df$VolumenSektion_ohne_Rinde <- as.numeric(sort_df$VolumenSektion_ohne_Rinde)
sort_df$LängeReal <- as.numeric(sort_df$LängeReal)
sort_df$LängeVK  <- as.numeric(sort_df$LängeVK)
sort_df$FahrerNr  <- as.numeric(sort_df$FahrerNr)
sort_df$BHD  <- as.numeric(sort_df$BHD)
sort_df$KoordinateNord  <- as.numeric(sort_df$KoordinateNord)
sort_df$KoordinateOst  <- as.numeric(sort_df$KoordinateOst)
sort_df$FußDurchmesser_mit_Rinde  <- as.numeric(sort_df$FußDurchmesser_mit_Rinde)
sort_df$FußDurchmesser_ohne_Rinde  <- as.numeric(sort_df$FußDurchmesser_ohne_Rinde)
Encoding(sort_df$Stärkeklasse) <- "UTF-8"
Encoding(sort_df$PRIDateiName) <- "UTF-8"
sort_df$BaumartIndex  <- as.numeric(sort_df$BaumartIndex)
sort_df$SortimentIndex  <- as.numeric(sort_df$SortimentIndex)
str(sort_df)

##replace umlauts
replace_str <- c('ü' = 'ue', 'ï' = 'ie', 'ë' = 'ee', 'ä' = 'ae','ö' = 'oe',
                 'Ü' = 'Ue', 'Ä' = 'Ae', 'Ö' = 'Oe', 'ß' = 'ss')
stringr::str_replace_all('üïëäöÄÜÖß', replace_str)

colnames(sort_df) <- stringr::str_replace_all(colnames(sort_df), replace_str)
sort_df$Baumart <- stringr::str_replace_all(sort_df$Baumart, replace_str)
sort_df$Sortiment <- stringr::str_replace_all(sort_df$Sortiment, replace_str)
sort_df$Staerkeklasse <- stringr::str_replace_all(sort_df$Staerkeklasse, replace_str)
sort_df$PRIDateiName <- stringr::str_replace_all(sort_df$PRIDateiName, replace_str)
str(sort_df)

write.csv2(sort_df, file = file.path(getwd(), "Daten/aufbereiteteDaten/Kaelberbronn/pri.csv"), row.names = F)



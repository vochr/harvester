#' @title read classic StanForD file into a list structure
#' @description read classic StanForD-Ascii-file into list for further
#' manipulation with subsequent functions
#' @param file a classic StanForD file like .stm, .pri, .prd and the like
#' @details supports only classic StanForD protocol as opposed to the
#' StanFordD 2010 protocol, which stores data using XML. In (classic) StanForD
#' data is stored in variables with types (e.g. var110_t2), which are numbered
#' from 1 to >1000, but generic variables are between 1 and 799. 800-899 and
#' 900-950 are reserved for testing, 1000-1999 are reserved for decimal
#' numerators. 2000-2999 are machine dependent variables in files .mas.
#' Multiple data entries of a single files, like several stems (with same
#' attributes) inside a .stm-file are considered (so far only this case).
#' @return a list of lists, holding each variable and type in a nested list.
#' Each variable is accessible via the respective list entry, e.g. var121 is
#' stored in ll[[121]], the specific type, e.g. var121_t1 in ll[[121]][[1]].
#' @examples
#' # production data
#' p <- "data/00100191608180615.prd"
#' prd <- readStanForD(file = p)
#' str(prd)
#'
#' # production-individual data (data of individual log and stem)
#' p <- "data/00100191608180615.pri"
#' pri <- readStanForD(file = p)
#' str(pri)
#'
#' # stem data, measured diameter and length
#' p <- "data/OPTIWIN2016-8-6_0003.stm"
#' stm <- readStanForD(file = p)
#' str(stm)
#'
#' # stem ID, identification of control stems
#' p <- "data/08061133.sti"
#' sti <- readStanForD(file = p)
#' str(sti)
#'
#' # bucking instructions and price matrices
#' p <- "data/00100191608180615.apt"
#' apt <- readStanForD(file = p)
#' str(apt)
#'
#' # operational monitoring data, time and repair data
#' p <- "data/20160818061450.drf"
#' drf <- readStanForD(file = p)
#' str(drf)
#' @export

readStanForD <- function(file){
  dat <- paste(readLines(con = file, warn=FALSE, encoding = "latin1"),
               collapse = " ---")
  dat <- strsplit(dat, split = "~", fixed = TRUE, useBytes = TRUE)[[1]]
  dat <- dat[!grepl("^[0-9]+ 0", dat)] # remove "non-variables" e.g. ~0 0 or ~901 0
  # head(dat)
  isString <- grepl(pattern = "---", dat, fixed = TRUE)
  ## get all variable_types combinations
  allVT <- sapply(1:length(dat), function(a){
    x <- strsplit(x = dat[a], split = " ", fixed = TRUE, useBytes = TRUE)[[1]]
    x <- paste0(x[c(1, 2)], collapse = " ")
  })
  vtn <- aggregate(allVT, by=list(vt=allVT), FUN="length")
  ## get number variable
  allVars <- as.integer(sapply(1:length(dat), function(a){
    strsplit(x = dat[a], split = " ", fixed = TRUE, useBytes = TRUE)[[1]][1]
  }))
  ## get number type
  allTypes <- as.integer(sapply(1:length(dat), function(a){
    strsplit(x = dat[a], split = " ", fixed = TRUE, useBytes = TRUE)[[1]][2]
  }))
  ## build data taking list of lists structure
  ll <- sapply(1:max(allVars), function(a){
    vector("list", max(c(allTypes[allVars==a], 1)))
  })
  for(i in seq(along=vtn$x)){
    # i <- 34
    (vt <- paste0("^", vtn[i,]$vt, " "))
    (tmp <- strsplit(vtn[i,]$vt, " ", fixed = TRUE, useBytes = TRUE)[[1]])
    (var <- as.integer(tmp[1]))
    (type <- as.integer(tmp[2]))
    (n <- vtn[i,]$x)
    (dati <- dat[grepl(vt, dat, useBytes = TRUE)])
    res <- vector("list", n)
    for(j in seq(n)){
      # j <- 1
      # all should be ok since the same var_type is processed each time
      if(all(isString[grepl(paste0("^", vtn[i,]$vt, " "), dat)])){
        (tmp <- strsplit(dati[j], " ---", fixed = TRUE, useBytes = TRUE)[[1]][-1])
        # as.Date(tmp, tryFormats = c("%y%m%d", "%y%m%d%H%M", "%y%m%d%H%M%S", "%Y%m%d%H%M%S"))
        res[[j]] <- tmp
      } else {
        (tmp <- strsplit(dati[j], "\\s+", useBytes = TRUE)[[1]][-c(1, 2)])
        if(any(as.numeric(tmp)>.Machine$integer.max)){
          ## this case should not happen since time is supposed to be a string,
          ## but in the test file (a real file from Ponsse) this is not the
          ## case, var318_t4 is stored as integer (no new line!)
          ## if integer is assumed, the given value is NA (to big in size),
          ## hence write into numeric
          tmp <- as.numeric(tmp)
        } else {
          tmp <- as.integer(tmp)
        }
        res[[j]] <- tmp
      }
    }

    if(n > 1){
      ll[[ var ]][[ type ]] <- res
    } else {
      ll[[ var ]][[ type ]] <- res[[1]]
    }

  }
  return(ll)
}

#' @title read StanForD STM-file from xml
#' @description Read xml-file which is generated from a .stm-file of Harvester
#' machines from forestry. .stm files comply with the StanForD protocol and
#' can be exported from the machines. So far, these .stm files need to be
#' transformed into a non-proprietary format, e.g. with software StanForD Report
#' available from german KWF.
#' @param file a xml file produced from a .stm file by StanForD Report
#' @param val function can return STMData, STMDataDetails or both, as a list
#' @details no details
#' @return a data.frame
#' @example
#' df <- readSTM(file = "test_stm.XML")
#' @importFrom XML parseXML xmlToList
#' @export

readSTM <- function(file, val = NULL){
  file <- "data/test_stm.xml"

  if(file.exists(file)){
    dat <- XML::xmlParse(file)
    dat <- XML::xmlToList(dat) #takes some time
  }

  if(is.null(val) | val == "STMData"){
    ## read and return STMData
    tree_list <- dat[which(names(dat) == "STMData")]
    tree_df <- data.frame(as.vector(t(sapply(tree_list, c))), row.names = NULL)
    colnames(tree_df) <- names(tree_list[[1]])[which(!sapply(tree_list[[1]], is.null))]
    tree_df$Stammnummer <- as.numeric(tree_df$Stammnummer)
    tree_df$BaumartCode <- as.numeric(tree_df$BaumartCode)
    tree_df$BHD <- as.numeric(tree_df$BHD)
    tree_df$BaumartCode <- as.numeric(tree_df$BaumartCode)
    str(tree_df)
    head(tree_df)

  } else if (is.null(val) | val == "STMDataDetails"){
    ## read and return STMDataDetails
    taper_list <- dat[which(names(dat) == "STMDataDetails")]
    taper_df <- data.frame(t(sapply(taper_list, function(a){as.vector(unlist(a))})),
                           row.names = NULL, stringsAsFactors = F)
    colnames(taper_df) <- c("ID_STMData", "Laenge", "Durchmesser", "SortimentName", "SortimentCode")
    taper_df$Laenge <- as.numeric(taper_df$Laenge)
    taper_df$Durchmesser <- as.numeric(taper_df$Durchmesser)
    taper_df$SortimentCode <- as.numeric(taper_df$SortimentCode)
    str(taper_df)
    head(taper_df)
  }
  if(is.null(val)){
    ll <- list(STMData = tree_df, STMDataDetails = taper_df)
  } else if(val == "STMData"){
    ll <- tree_df
  } else if(val == "STMDataDetails"){
    ll <- taper_df
  }
  return(ll)
}

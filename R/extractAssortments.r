#' @title extract assortment-data from .pri-file after read in
#' @description extract the assortment data from a list of lists which was
#' generated from a .pri file via \code{\link{readStanForD}}
#' @param pri a list generated by \code{\link{readStanForD}} from a .pri-file
#' @details The function extracts log data (var257_1) and stem data (var267_1),
#' merges both and reshapes the results into a data.frame. The provided columns
#' are dependent on the .pri file but all columns are returned. Logs without
#' assortment specification ('') become 'unclassified'.
#' Column names according to the Appendix of the Standard for Forest Data and
#' Communication (StanForD).
#' @return a data.frame
#' @examples
#' p <- "data/00100191608180615.pri"
#' pri <- readStanForD(file = p)
#' assort <- extractAssortments(x=pri)
#' assort$ass <- ifelse(assort$assortment %in% c("X-Holz", "unclassified"), 0, 1)
#' aggregate(assort[, c("LengthReal", "Voldlsmiub")], by=list(assort$ass),
#'           FUN = "sum")
#' p <- "data/OPTIWIN2016-8-6_0003.stm"
#' stm <- readStanForD(file = p)
#' extractAssortments(stm)
#' @export

extractAssortments <- function(x){

  if(x[[1]][[2]] == "PRI"){
    #### now processing PRI files ####
    pri <- x
    var255 <- c(AssortNo=1, SpCode=2, uID=20, topD_ob=201, topD_ub=202, midD_ob=203,
                midD_ub=204, rootD_ob=205, rootD_ub=206, midD_ob_HKS=207, midD_ub_HKS=208,
                forcedCut=300, LengthReal=301, LengthClass=302, Vol=400, Vold=1400,
                Vol_m3sob=401, Vol_m3sobd=1401, Vol_m3sub=402, Vol_m3subd=1402,
                Vol_m3topob=403, Vol_m3topobd=1403, Vol_m3topub=404, Vol_m3topubd=1404,
                Vol_m3smiob=405, Vol_m3smiobd=1405, Vol_m3smiub=406, Vol_m3smiubd=1406,
                Voldl=420, Voldlsob=421, Voldlsub=422, Voldltopob=423, Voldltopub=424,
                Voldlsmiob=425, Voldlsmiub=426, StemNo=500, LogNo=501, NofLogs=600)
    var266 <- c(SpCode=2, StemNo=500, BioEnergy=505, RefDBH=723, HDBH=724,
                DBH=740, StemType=741, OperatorNo=750, Lat=760, NS=761,
                Long=762, EW=763, Altitude=764)
    ## restructure data # logs
    df1 <- data.frame(matrix(pri[[257]][[1]], ncol=pri[[255]][[1]], byrow = TRUE))
    colnames(df1) <- sapply(1:pri[[255]][[1]], function(a) {
      names(which(var255 == pri[[256]][[1]] [a]))
    })

    df1$AssortNo <- ifelse(df1$AssortNo==0, length(pri[[121]][[1]]), df1$AssortNo)
    df1$assortment <- pri[[121]][[1]][df1$AssortNo]
    df1$assortCode <- pri[[121]][[2]][df1$AssortNo]
    df1$AssortNo <- ifelse(df1$AssortNo==length(pri[[121]][[1]]), 0, df1$AssortNo)

    ## restructure data # trees
    df2 <- data.frame(matrix(pri[[267]][[1]], ncol=pri[[265]][[1]], byrow=TRUE))
    colnames(df2) <- sapply(1:pri[[265]][[1]], function(a) {
      names(which(var266 == pri[[266]][[1]] [a]))
    })
    df2$NS <- ifelse(df2$NS == 1, "N", "S")
    df2$EW <- ifelse(df2$EW == 1, "E", "W")
    df2$Species <- (pri[[120]][[1]])[df2$SpCode]
    df2$SpCode <- (pri[[120]][[3]])[df2$SpCode]

    ## merge results
    df <- merge(df1, df2, by = "StemNo")
    return(df)

  } else if(x[[1]][[2]] == "STM"){
    #### now processing stm files ####
    stm <- x

    # number of trees in file
    (n <- length(stm[[110]][[1]]) + length(stm[[110]][[2]]))
    (sp <- unlist(c(stm[[110]][[1]], stm[[110]][[2]]))) # species index of var120
    (StemNo <- unlist(stm[[270]][[1]])) # stem number
    (StemID <- unlist(stm[[270]][[3]])) # stem number (unique for harvesting object/location)
    if(!is.null(stm[[281]][[1]])){
      (dbh <- unlist(stm[[281]][[1]])) # dbh
    } else {
      ## extract from taper measurements
      start <- stm[[271]][[2]] # in cm
      end <- stm[[272]][[2]] # in cm
      step <- stm[[269]][[2]] # in cm
      dbh <- rep(NA, n)
    }
    (nlogs <- unlist(stm[[290]][[1]]))
    (topDob <- stm[[291]][[5]]) # measured by machine
    (topDub <- stm[[292]][[5]]) # based on diameter measured by machine???
    (l <- stm[[293]][[5]]) # measured by machine
    (assort <- stm[[296]][[2]])
    (assortCode <- stm[[296]][[3]])
    (assortpm <- stm[[296]][[4]])
    (volpm <- stm[[299]][[1]]) # vol according to price matrix
    (volub <- stm[[299]][[2]]) # solid vol under bark
    (volob <- stm[[299]][[3]]) # solid vol over bark as measured by machine

    ## transform assortpm into readable text (instead of code)

    ## extract, manipulate and merge data
    df <- data.frame(StemNo=numeric(0), StemID=numeric(0), SpCode=character(0),
                     SpName=character(0), dbh=numeric(0), LogNo=numeric(0),
                     assortName=character(0), assortCode=character(0),
                     l=numeric(0), topDob=numeric(0), topDub=numeric(0),
                     volpm=numeric(0), assortpm=numeric(0), volub=numeric(0),
                     volob=numeric(0))
    for(i in seq(n)){
      # i <- 1
      spc <- ifelse(length(stm[[120]])>=3, stm[[120]][[3]][sp[i]], NA)
      spn <- ifelse(!is.null(stm[[120]][[1]]), stm[[120]][[1]][sp[i]], NA)
      if(is.na(dbh[i])){
        h <- seq(start[[i]], end[[i]]-ifelse(end[[i]]%%step[[i]]==0, step[[i]], 0), step[[i]])
        dbh[i] <- (stm[[273]][[2]][[i]])[which(h == 130)]
      }
      tmp <- data.frame(StemNo=StemNo[i], StemID=StemID[i], SpCode=spc,
                        SpName=spn, dbh=dbh[i],
                        LogNo=seq(nlogs[i]), assortName=assort[[i]],
                        assortCode=assortCode[[i]], l=l[[i]], topDob=topDob[[i]],
                        topDub=topDub[[i]], volpm=volpm[[i]],
                        assortpm=assortpm[[i]], volub=volub[[i]], volob=volob[[i]])
      df <- rbind(df, tmp)
    }

    return(df)

  } else {

    stop("'list' was not generated from PRI-file nor from STM-file!")

  }

}
